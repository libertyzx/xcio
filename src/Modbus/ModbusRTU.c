/*=========================================================|
 | 文件名:  ModbusRTU.c
 | 描述:    ModbusRTU协议处理
 | 版本:    V1.02
 | 日期:    2022/12/30
 | 语言:    C语言
 | 作者:    libertyzx
 | E-mail:  libertyzx@163.com
 +-----------------------------------------------|
 | 开源协议: MIT License
 +-----------------------------------------------|
 +--- 说明
 |  1.ModbusRTU协议实现
 |  2.RTU帧格式
 |      /====================================\
 |      | 目标地址 + 功能码 +-- 数据 --+ CRC |
 |      |  1B      +  1B    +  0-252B  + 2B  |
 |      \====================================/
 |  3.地址说明
 |      广播地址是:0;
 |      子节点地址:1-247;
 |      保留地址是:248-255;
 |      注意:主节点没有地址;
 |  4.超时时间ModbusRTU标准是3.5个字节时间,
 |      此库默认按5ms计算若是2个字节传输时间超5ms表示一个包结束;
 |      此配置可改;
 +-----------------------------------------------|
 +--- 版本说明
 |  V1.01:-2022/12/16
 |      1.初始化
 |  V1.02:-2022/12/30
 |      1.优化超时时间处理函数
 *========================================================*/
//=== 头文件
    #include "ModbusRTU.h"
    #include "CRC.h"
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 私有类型 ===================================|

//=== 私有宏定义 =================================|
    //状态机状态
    #define _FSM_Addr       /*地址*/        (0)
    #define _FSM_Func       /*功能码*/      (1)
    #define _FSM_Data       /*数据*/        (2)
    #define _FSM_CRC        /*校验*/        (3)
    #define _FSM_DataExt    /*数据扩展*/    (4)
    //广播地址
    #define _BroadcastAddr   /*广播地址是0x00*/  0x00;
    //公共功能码
    #define _Func_01H   /*读线圈*/          (0x01)
    #define _Func_02H   /*读离散量输入*/    (0x02)
    #define _Func_03H   /*读保持寄存器*/    (0x03)
    #define _Func_04H   /*读输入寄存器*/    (0x04)
    #define _Func_05H   /*写单个线圈*/      (0x05)
    #define _Func_06H   /*写单个寄存器*/    (0x06)
    #define _Func_0FH   /*写多个线圈*/      (0x0F)
    #define _Func_10H   /*写多个寄存器*/    (0x10)
    #define _Func_14H   /*读文件记录*/      (0x14)
    #define _Func_15H   /*写文件记录*/      (0x15)
    #define _Func_16H   /*屏蔽写寄存器*/    (0x16)
    #define _Func_17H   /*读/写多个寄存器*/ (0x17)
    #define _Func_2BH   /*读设备识别码*/    (0x2B)

//=== 私有宏定义配置 =============================|
//=== 私有函数宏 =================================|
    //复位解码状态机
    #define _RstDecodeFSM() \
        do{     \
            ph->NextDataLen = 0;            /*清除下个数据长度*/    \
            ph->FSMState    = _FSM_Addr;    /*复位状态*/    \
            ph->Time        = 0;            /*清除时间*/    \
        }while(0)

//=== 私有全局变量 ===============================|
//=== 私有函数 ===================================|
    //弱函数
    extern void ModbusRTU_Weak_DataProcessing(HandleModbusRTU *ph, uchar FunCode, uchar *pAddr, uchar Len);   //接收解析ok数据处理
    extern void ModbusRTU_Weak_UpdateTime(HandleModbusRTU *ph);     //更新当前时间
    extern int ModbusRTU_Weak_EndFrameTimeout(HandleModbusRTU *ph); //帧结束判断
    //弱函数-钩子
    extern int ModbusRTU_Hook_RxNodeAddr(HandleModbusRTU *ph, uchar NodeAddr);  //接收节点地址处理
    extern int ModbusRTU_Hook_RxFunCode(HandleModbusRTU *ph, uchar NodeAddr);   //接收功能码处理
    //私有
    static int ModbusRTU_RxFunCode(HandleModbusRTU *ph, uchar FunCode);   //功能码处理

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//===

/************************************************|
 * 描述:    初始化
 * 函数名:  ModbusRTU_Init
 * 参数[I]: HandleModbusRTU *ph
 * 参数[I]: uchar ID                //不参与运算,只为了方便用户处理数据
 * 参数[I]: uchar *pCmdBufAddr      //指令缓存
 * 参数[I]: ushort CmdBufSize       //指令缓存大小(最大0xFF)
 * 参数[I]: uchar *pFIFOBufAddr     //队列缓存
 * 参数[I]: ushort FIFOBufSize      //队列缓存大小(16位)
 * 返回:    void
 *** 说明:  设置ModbusRTU缓存,必须在串口初始化前初始化,防止错误
 *** 例程:  无
 ************************************************/
void ModbusRTU_Init(HandleModbusRTU *ph,
    uchar ID,
    uchar *pCmdBufAddr, uchar CmdBufSize,
    uchar *pFIFOBufAddr, ushort FIFOBufSize)
{
    //指令缓存
    ph->pCmdBufAddr = pCmdBufAddr;
    ph->CmdBufSize  = CmdBufSize;
    //数据处理
    ph->ID          = ID;               //句柄ID
    ph->NextDataLen = 0;                //下个数据长度
    ph->FSMState    = _FSM_Addr;        //状态状态
    ph->NodeAddr    = _BroadcastAddr;   //节点地址,默认广播;
    ph->ExtDataLen  = 0;                //扩展数据长度
    ph->PackLen     = 0;                //包长度
    //队列设置
    FIFO_Create(&ph->FIFO, 1, pFIFOBufAddr, FIFOBufSize); //创建一个队列
}

/************************************************|
 * 描述:    设置接收节点地址
 * 函数名:  ModbusRTU_SetRxNodeAddr
 * 参数[I]: HandleModbusRTU *ph
 * 参数[I]: uchar NodeAddr      //设置的节点地址
 * 返回:    void
 *** 说明:  接收前先设定地址,不对的地址不会处理
 ***    注意:
 ***        "ModbusRTU_Hook_NodeAddr"钩子可以改变接收规则;
 ***        地址是广播地址的时候.所有地址都会处理;
 *** 例程:  无
 ************************************************/
void ModbusRTU_SetRxNodeAddr(HandleModbusRTU *ph, uchar NodeAddr)
{
    ph->NodeAddr = NodeAddr;
}

/************************************************|
 * 描述:    数据解码
 * 函数名:  ModbusRTU_Decode
 * 参数[I]: HandleModbusRTU *ph
 * 返回:    int
 *** 说明:  需要放到循环中,或者定时调用;
 *** 例程:  无
 ************************************************/
int ModbusRTU_Decode(HandleModbusRTU *ph)
{
    ushort Len;
    int Ret;
    uchar Buf[2];
    HandleFIFO *phFIFO;
    int NextLen;
    ushort Cache16;
    ushort CRC16;
//===
    phFIFO = &ph->FIFO;
    Len = FIFO_GetLen(phFIFO);            //-得到数据长度
    if(Len == 0){ return(_ModbusRTU_R_NoData); }
    //比较时间是否错误
    if(ModbusRTU_Weak_EndFrameTimeout(ph) == 1){
        //帧结束
        _RstDecodeFSM();
    }
    //处理数据
    while(Len >= ph->NextDataLen){      //判断单次处理后长度是否有效
        switch(ph->FSMState){
            //判断地址是否ok
            case _FSM_Addr:
                Ret = FIFO_ShiftExt(phFIFO, Buf, 1);    //出1Byte
                if(Ret != 1){ goto GOTO_Error;}         //-没有数据跳出
                Len--;
                //地址判断
                Ret = ModbusRTU_Hook_RxNodeAddr(ph, Buf[0]);  //地址处理钩子
                if( (Ret == 1) ||                   //钩子接管
                    (ph->NodeAddr == Buf[0]) ||     //地址效验OK
                    (ph->NodeAddr == 0) ){          //0号地址,直接接收;
                    //更新状态
                    ph->FSMState = _FSM_Func;       //指向状态码
                    ph->NextDataLen = 1;            //下个数据大小
                    ph->pCmdBufAddr[0] = Buf[0];    //保存数据
                    ph->PackLen = 1;                //包长
                }
                break;
            //功能码
            case _FSM_Func:
                Ret = FIFO_ShiftRead(phFIFO, 0, Buf, 1);      //只读功能码
                if(Ret != 0){ goto GOTO_Error;}                 //-没有数据跳出
                NextLen = ModbusRTU_Hook_RxFunCode(ph, Buf[0]);   //钩子-功能码操作
                if(NextLen == 0){
                    NextLen = ModbusRTU_RxFunCode(ph, Buf[0]);    //功能码操作
                }
                //数据判断
                if(NextLen == 0){
                    //功能码错误
                    _RstDecodeFSM();    //复位状态机
                }
                else{
                    ph->FSMState = _FSM_Data;       //指向状态码
                    ph->pCmdBufAddr[1] = Buf[0];    //保存数据
                    ph->NextDataLen += NextLen;     //数据长度+下个数据大小
                    ph->PackLen += 1;               //+功能码
                }
                break;
            //等待数据
            case _FSM_Data:
                Cache16 = ph->NextDataLen-1;
                Ret = FIFO_ShiftRead(phFIFO, 1, &ph->pCmdBufAddr[2], Cache16);  //只读
                if(Ret != 0){ goto GOTO_Error; }            //-没有数据跳出
                ph->PackLen += Cache16;                     //+数据长度
                if(ph->ExtDataLen){
                    //有扩展数据
                    ph->ExtDataLen = ph->pCmdBufAddr[2];    //扩展数据长度
                    ph->NextDataLen += ph->ExtDataLen;      //下个数据大小
                    ph->FSMState = _FSM_DataExt;            //指向状态码
                }
                else{
                    //正常到校验
                    ph->NextDataLen += 2;       //下个数据大小
                    ph->FSMState = _FSM_CRC;    //指向状态码
                }
                break;
            //数据扩展
            case _FSM_DataExt:
                Ret = FIFO_ShiftRead(phFIFO, ph->PackLen-1, &ph->pCmdBufAddr[ph->PackLen], ph->ExtDataLen); //只读
                if(Ret != 0){ goto GOTO_Error; }    //-没有数据跳出
                ph->PackLen += ph->ExtDataLen;      //+扩展数据长度
                ph->NextDataLen += 2;               //下个数据大小
                ph->FSMState = _FSM_CRC;            //指向状态码
                break;
            //等待校验
            case _FSM_CRC:
                Ret = FIFO_ShiftRead(phFIFO, ph->PackLen-1, Buf, 2); //只读
                if(Ret != 0){ goto GOTO_Error; }    //-没有数据跳出
                CRC16 = CRC16_ModbusRTU(ph->pCmdBufAddr, ph->PackLen);
                Cache16 = Buf[0] | (Buf[1]<<8);
                if(CRC16 != Cache16){
                    //校验错误
                    _RstDecodeFSM();
                    break;
                }
                //校验成功-数据处理
                ph->pCmdBufAddr[ph->PackLen++] = Buf[0];
                ph->pCmdBufAddr[ph->PackLen++] = Buf[1];
                FIFO_Shift(phFIFO, NULL, ph->PackLen-1);    //抛弃数据(地址位之前就已经抛弃)
                ModbusRTU_Weak_DataProcessing(ph, ph->pCmdBufAddr[1], &ph->pCmdBufAddr[2], ph->PackLen-2);
                _RstDecodeFSM();
                return(_ModbusRTU_R_OK);    //完成一个帧解析
            //无效状态,错误
            default:
                goto GOTO_Error;
        }
        //时间处理
        ModbusRTU_Weak_UpdateTime(ph);
    }
    //等待下个数据
    return(_ModbusRTU_R_Continue);
//===
GOTO_Error:
    _RstDecodeFSM();
    return(_ModbusRTU_R_FIFO);          //=队列错误
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */

/************************************************|
 * 描述:    [私有]发送组包
 * 函数名:  ModbusRTU_Pack
 * 参数[I]: uchar *pSendBufAddr //发送组包缓存
 * 参数[I]: uchar SendBufSize   //发送组包缓存最大大小
 * 参数[I]: uchar Addr          //发送节点地址
 * 参数[I]: uchar FunCode       //发送功能码
 * 参数[I]: uchar *pDataAddr    //发送的数据
 * 参数[I]: uchar DataLen       //发送数据的长度
 * 返回:    int                 //返回组包后的长度
 *** 说明:  要发送的数据组包,自行选择硬件设备发送;
 *** 例程:  无
 ************************************************/
int ModbusRTU_Pack(
    uchar *pSendBufAddr, uchar SendBufSize,
    uchar Addr, uchar FunCode,
    uchar *pDataAddr, uchar DataLen)
{
    uchar *p;
    uchar i;
    ushort CRC16;
//===
    if((DataLen+2+2) > SendBufSize){
        return(0);      //超缓存
    }
    p = pSendBufAddr;
    *p++ = Addr;        //地址
    *p++ = FunCode;     //功能码
    //数据
    i = DataLen;
    while(i--){
        *p++ = *pDataAddr++;
    }
    //校验
    CRC16 = CRC16_ModbusRTU(pSendBufAddr, DataLen+2);
    *p++ = CRC16 & 0xFF;
    *p++ = (CRC16>>8) & 0xFF;

//===
    return(DataLen+2+2);
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */

/************************************************|
 * 描述:    [私有]功能码处理
 * 函数名:  ModbusRTU_RxFunCode
 * 参数[I]: HandleModbusRTU *ph
 * 参数[I]: uchar FunCode        //功能码
 * 返回:    int             //返回数据数量
 *** 说明:  按功能码输出字节
 *** 例程:  无
 ************************************************/
static int ModbusRTU_RxFunCode(HandleModbusRTU *ph, uchar FunCode)
{
    uchar Len = 0;
//===
    ph->ExtDataLen = 0;                 //扩展长度=0;
    if(FunCode & 0x80){
        if(FunCode == (0x85 | 0x80)){
            return(2);                  //设备识别码错误有2个字节
        }
        return(1);                      //=错误,设备会输出一个异常码;
    }
    //功能码处理
    switch(FunCode){
        case _Func_01H: Len=1; ph->ExtDataLen=1; break; /*读线圈*/
        case _Func_02H: Len=1; ph->ExtDataLen=1; break; /*读离散量输入*/
        case _Func_03H: Len=1; ph->ExtDataLen=1; break; /*读保持寄存器*/
        case _Func_04H: Len=1; ph->ExtDataLen=1; break; /*读输入寄存器*/
        case _Func_05H: Len=4; ph->ExtDataLen=0; break; /*写单个线圈*/
        case _Func_06H: Len=4; ph->ExtDataLen=0; break; /*写单个寄存器*/
        case _Func_0FH: Len=4; ph->ExtDataLen=0; break; /*写多个线圈*/
        case _Func_10H: Len=4; ph->ExtDataLen=0; break; /*写多个寄存器*/
        case _Func_14H: Len=1; ph->ExtDataLen=1; break; /*读文件记录*/
        case _Func_15H: Len=1; ph->ExtDataLen=1; break; /*写文件记录*/
        case _Func_16H: Len=6; ph->ExtDataLen=0; break; /*屏蔽写寄存器*/
        case _Func_17H: Len=1; ph->ExtDataLen=1; break; /*读/写多个寄存器*/
        case _Func_2BH: Len=9; ph->ExtDataLen=0; break; /*读设备识别码*/
        default:
            break;
    }
//===
    return(Len);
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//===操作
/************************************************|
 * 描述:    输入数据
 * 函数名:  ModbusRTU_InData
 * 参数[I]: HandleModbusRTU *ph
 * 参数[I]: uchar* pAddr        //输入的数据
 * 参数[I]: ushort Len          //输入数据的长度
 * 返回:    void
 *** 说明:  从总线上读取数据;
 ***    总线上得到数据由此函数写入"ModbusRTU"缓存;
 ***    数据读取和处理是异步的,数据处理在"ModbusRTU_Task"中;
 *** 例程:  无
 ************************************************/
int ModbusRTU_InData(HandleModbusRTU *ph, uchar* pAddr, ushort Len)
{
    return(FIFO_Push(&ph->FIFO, pAddr, Len));
}

/************************************************ 我是分割线 ************************************************/
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
#if(_ModbusRTU_WeakFunc_EN == 1)
/*=== 弱函数实现-说明*/
/**
 */

/************************************************|
 * 描述:    [弱函数]解析完成后数据处理
 * 函数名:  ModbusRTU_Weak_DataProcessing
 * 参数[H]: HandleModbusRTU *ph
 * 参数[I]: uchar FunCode   //功能码
 * 参数[I]: uchar *pAddr    //数据
 * 参数[I]: uchar Len       //数据长度
 * 返回:    void
 *** 说明:  用户处理数据
 *** 例程:  无
 ************************************************/
__WEAK void ModbusRTU_Weak_DataProcessing(HandleModbusRTU *ph, uchar FunCode, uchar *pAddr, uchar Len)
{}

/************************************************|
 * 描述:    [弱函数]更新当前时间
 * 函数名:  ModbusRTU_Weak_UpdateTime
 * 参数[H]: HandleModbusRTU *ph
 * 返回:    void
 *** 说明:  用于更新当前时间,用来处理ModbusRTU帧结束
 *** 例程:  无
 ************************************************/
__WEAK void ModbusRTU_Weak_UpdateTime(HandleModbusRTU *ph)
{
    ph->Time = _Time_GetTick();     //得到当前系统Tick
}

/************************************************|
 * 描述:    [弱函数]帧结束判断
 * 函数名:  ModbusRTU_Weak_EndFrameTimeout
 * 参数[H]: HandleModbusRTU *ph
 * 返回:    int     //0:没有结束; 1:帧结束;
 *** 说明:
 ***    配合"ModbusRTU_Weak_UpdateTime"使用,
 ***    用于判断,ModbusRTU帧结束;
 ***    调用"ModbusRTU_Weak_UpdateTime"和"ModbusRTU_Weak_EndFrameTimeout"时间超5ms(默认可配置)则表示帧结束;
 *** 例程:  无
 ************************************************/
__WEAK int ModbusRTU_Weak_EndFrameTimeout(HandleModbusRTU *ph)
{
    if( (ph->Time != 0) && _Time_CompareTick_ms(ph->Time, _ModbusRTU_EndFrameTimeout) ){
        ph->Time = 0;
        return(1);      //帧超时
    }
    return(0);
}

//===
#endif

/************************************************ 我是分割线 ************************************************/
#if(_ModbusRTU_HookFunc_EN == 1)
/*=== 钩子实现-说明*/
/**
 */

/************************************************|
 * 描述:    [钩子]-接收到节点地址处理
 * 函数名:  ModbusRTU_Hook_RxNodeAddr
 * 参数[I]: HandleModbusRTU *ph
 * 参数[I]: uchar NodeAddr  //节点地址
 * 返回:    int             //0:正常处理;1:本次地址契合,继续解析;
 *** 说明:  钩子处理会覆盖原有地址处理规则
 *** 例程:  无
 ************************************************/
__WEAK int ModbusRTU_Hook_RxNodeAddr(HandleModbusRTU *ph, uchar NodeAddr)
{
    return(0);
}

/************************************************|
 * 描述:    [钩子]-接收到功能码处理
 * 函数名:  ModbusRTU_Hook_RxFunCode
 * 参数[I]: HandleModbusRTU *ph
 * 参数[I]: uchar FunCode   //功能码
 * 返回:    int             //0:正常处理;>0:输出要处理的数据有多少;
 *** 说明:  钩子处理会覆盖原有地址处理规则
 *** 例程:  无
 ************************************************/
__WEAK int ModbusRTU_Hook_RxFunCode(HandleModbusRTU *ph, uchar NodeAddr)
{
    return(0);
}


//===
#endif


/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
