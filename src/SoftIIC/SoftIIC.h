/*=========================================================|
 | 文件名:  SoftIIC.h
 | 描述:    软件模拟IIC-函数声明
 | 版本:    V0.01
 | 日期:    2018/11/12
 | 语言:    C语言
 | 作者:    libertyzx
 | E-mail:  libertyzx@163.com
 +-----------------------------------------------|
 | 开源协议: MIT License
 +-----------------------------------------------|
 +--- 说明
 |  1.软件模拟IIC
 +-----------------------------------------------|
 +--- 版本说明
 |  V0.01:-2018/11/12
 |      1.见".c"文件
 *========================================================*/
//=== 防重复定义
#ifndef _SoftIIC_H_
#define _SoftIIC_H_
//=== 头文件
    #include "XCBase.h"

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */

//=== 数据配置宏 ==========================================|
    //-是否开启弱函数(1:开;0:关)
    #ifndef _SoftIIC_WeakFunc_EN
        #define _SoftIIC_WeakFunc_EN     (1)
    #endif

//=== 100KBps延时 ================================|
    //-100K(>4.7us);400K(>1.3)
    //-100K(>4.0us);400K(>0.6)
    //-起始信号延时
    #ifndef _SoftIIC_STsdaH
        #define _SoftIIC_STsdaH /*起始信号SDA保持高时间(>4.7us)*/  (5)
    #endif
    #ifndef _SoftIIC_STsdaL
        #define _SoftIIC_STsdaL /*起始信号SDA保持低时间(>4.0us)*/  (4)
    #endif

    //-结束信号延时
    #ifndef _SoftIIC_PTsdaL
        #define _SoftIIC_PTsdaL /*结束信号SDA保持低时间(>4.0us)*/  (4)
    #endif
    #ifndef _SoftIIC_PTsdaH
        #define _SoftIIC_PTsdaH /*结束信号SDA保持高时间(>4.7us)*/  (5)
    #endif

    //-时钟信号延时
    #ifndef _SoftIIC_DTsclH
        #define _SoftIIC_DTsclH /*数据传输时钟高电平时间(>4.0us)*/ (4)
    #endif

    //-时钟低-改变数据的延时
    #ifndef _SoftIIC_DTf
        #define _SoftIIC_DTf    /*数据改变时前半个时钟(>2.3us)*/   (2)
    #endif
    #ifndef _SoftIIC_DTb
        #define _SoftIIC_DTb    /*数据改变时后半个时钟(>2.3us)*/   (2)
    #endif

    //-获取从机ACK时在释放SDL总线后延时等待从机ACK时间
    #ifndef _SoftIIC_ATdev
        #define _SoftIIC_ATdev  /*一般半个低电平时间(>2.3us)*/     (3)
    #endif

/************************************************ 我是分割线 ************************************************/
//=== 宏定义|枚举变量 =====================================|

/************************************************ 我是分割线 ************************************************/
//=== 数据类型 ============================================|
/*-软件IIC传输类型*/
    typedef volatile struct{
        uchar *pBuf;        //-读写数据缓存
        uchar  WriteLen;    //-写数据长度
        uchar  ReadLen;     //-读数据长度
        uchar  DevAddr;     //-设备地址(8位,最低位为0,不左移1位)
    }TypeSoftIIC;

/*-软件IIC传输类型-指定地址读写数据*/
    typedef volatile struct{
        uchar *pBuf;        //-读写数据缓存
        uchar  DevAddr;     //-设备地址(8位,最低位为0,不左移1位)
        uchar  Len;         //-读写长度
        ushort Addr;        //-地址
    }TypeSoftIIC_Addr;

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 函数声明 ============================================|
/*=== 初始化*/
    void SoftIIC_Init(uchar ID);

/*=== 基础函数*/
    void SoftIIC_Start(uchar ID);                   //启动信号[主机发起]
    void SoftIIC_Stop(uchar ID);                    //停止信号[主机发起]
    uchar SoftIIC_GetSlaveACK(uchar ID);            //从机应答信号[从机发起]
    void SoftIIC_ACK(uchar ID);                     //主机应答信号[主机发起]
    void SoftIIC_NACK(uchar ID);                    //主机非应答信号[主机发起]
    void SoftIIC_TxByte(uchar ID, uchar Byte);      //主机发送字节[主机发起]
    uchar SoftIIC_RxByte(uchar ID);                 //主机接收字节[主机发起]

/*=== 应用函数*/
    uchar SoftIIC_ReadWrite(uchar ID, TypeSoftIIC *pData);                  //读写数据
    uchar SoftIIC_Write(uchar ID, uchar DevAddr, uchar *pData, uchar Len);  //写数据
    uchar SoftIIC_Read(uchar ID, uchar DevAddr, uchar *pData, uchar Len);   //读数据

/*=== 应用函数-指定地址读写*/
    uchar SoftIIC_Read_Specify8BitAddr(uchar ID, TypeSoftIIC_Addr *pt);     //读
    uchar SoftIIC_Write_Specify8BitAddr(uchar ID, TypeSoftIIC_Addr *pt);    //写

//=== 函数宏 ==============================================|
/*=== 函数宏类型*/

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 文件结束
#endif
