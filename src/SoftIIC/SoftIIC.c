/*=========================================================|
 | 文件名:  SoftIIC.c
 | 描述:    软件IIC
 | 版本:    V0.01
 | 日期:    2018/11/12
 | 语言:    C语言
 | 作者:    libertyzx
 | E-mail: libertyzx@163.com
 +-----------------------------------------------|
 | 开源协议: MIT License
 +-----------------------------------------------|
 +--- 说明
 |  1.软件IIC
 +-----------------------------------------------|
 +--- 版本说明
 |  V0.01:-2018/11/12
 |      1.初始化
 *========================================================*/
//=== 头文件
    #include "SoftIIC.h"
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 私有类型 ============================================|
//=== 私有数据配置宏 ======================================|
//=== 私有宏定义 ==========================================|
    #define _DevDir_Write   /*设备写*/               (0x00)
    #define _DevDir_Read    /*设备读*/               (0x01)
//=== 私有函数宏定义 ======================================|
//=== 私有全局变量 ========================================|
//=== 私有函数 ============================================|
//=== 外部函数 ============================================|
    //-弱函函数
    extern void SoftIIC_WeakIO_SDAOut(uchar ID, uchar HL);          //-[弱函数]数据线输出
    extern uchar SoftIIC_WeakIO_SDAIn(uchar ID);                    //-[弱函数]数据线输入
    extern void SoftIIC_WeakIO_SDARelease(uchar ID, uchar Param);   //-[弱函数]数据线释放
    extern void SoftIIC_WeakIO_SCLOut(uchar ID, uchar HL);          //-[弱函数]时钟线输出
    extern void SoftIIC_Weak_Delay_us(uchar ID, ushort us);         //-[弱函数]us延时

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/*=== 初始化*/
/************************************************|
 * 描述   : 初始化
 * 版本   : 0.01
 * 日期   : 2018/11/12
 * 函数名 : SoftIIC_Init
 * 参数   : uchar ID      //-总线ID
 * 返回   : void
 *** 说明 : 无
 *** 例程 : 无
 ************************************************/
void SoftIIC_Init(uchar ID)
{
    SoftIIC_WeakIO_SCLOut(ID, 1);
    SoftIIC_WeakIO_SDAOut(ID, 1);
}
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/*=== IIC基础函数*/
/************************************************|
 * 描述   : 启动信号[主机发起]
 * 版本   : 0.01
 * 日期   : 2018/11/12
 * 函数名 : SoftIIC_Start
 * 参数   : uchar ID      //-总线ID
 * 返回   : void
 *** 说明 : 无
 *** 例程 : 无
 ************************************************/
void SoftIIC_Start(uchar ID)
{
    SoftIIC_WeakIO_SDAOut(ID, 1);
    SoftIIC_WeakIO_SCLOut(ID, 1);
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_STsdaH);
    SoftIIC_WeakIO_SDAOut(ID, 0);                       //-在SCL高的时候下降沿-启动
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_STsdaL);
    SoftIIC_WeakIO_SCLOut(ID, 0);                       //-时钟下拉,准备发送/接受数据
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTf);             //-低电平前半个延时
}

/************************************************|
 * 描述   : 停止信号[主机发起]
 * 版本   : 0.01
 * 日期   : 2018/11/12
 * 函数名 : SoftIIC_Stop
 * 参数   : uchar ID      //-总线ID
 * 返回   : void
 *** 说明 : 无
 *** 例程 : 无
 ************************************************/
void SoftIIC_Stop(uchar ID)
{
    //-时钟低-数据低
    SoftIIC_WeakIO_SDAOut(ID, 0);
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTb);             //-低电平后半个延时

    //-时钟高
    SoftIIC_WeakIO_SCLOut(ID, 1);                       //-拉高时钟准备停止
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_PTsdaL);
    SoftIIC_WeakIO_SDAOut(ID, 1);                       //-SDA[0->1]-停止
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_PTsdaH);
}

/************************************************|
 * 描述   : 从机应答信号[从机发起]
 * 版本   : 0.01
 * 日期   : 2018/11/12
 * 函数名 : SoftIIC_GetSlaveACK
 * 参数   : uchar ID      //-总线ID
 * 返回   : uchar
 *  +=返回ACK状态
 *  |- 0: ACK
 *  |- 1: NACK
 *** 说明 : 主机向从机发送数据,从机应答
 *** 例程 : 无
 ************************************************/
uchar SoftIIC_GetSlaveACK(uchar ID)
{
    uchar  State;
//===
    //-时钟低-释放SDL
    SoftIIC_WeakIO_SDARelease(ID, 0);                   //-数据线-释放
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_ATdev);           //-等待收到ACK
    /*这里可以做等待设备ACK的超时*/

    //-高电平获取设备ACK
    SoftIIC_WeakIO_SCLOut(ID, 1);
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTsclH);
    State = SoftIIC_WeakIO_SDAIn(ID);                   //-获取设备ACK

    //-时钟低-恢复SDL控制
    SoftIIC_WeakIO_SCLOut(ID, 0);
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTf);
    SoftIIC_WeakIO_SDARelease(ID, 1);                   //-数据线-控制
//===
    return(State);
}

/************************************************|
 * 描述   : 主机应答信号[主机发起]
 * 版本   : 0.01
 * 日期   : 2018/11/12
 * 函数名 : SoftIIC_ACK
 * 参数   : uchar ID      //-总线ID
 * 返回   : void
 *** 说明 : 主机获取从机数据,反馈从机
 *** 例程 : 无
 ************************************************/
void SoftIIC_ACK(uchar ID)
{
    //-时钟低
    SoftIIC_WeakIO_SDAOut(ID, 0);                       //-应答
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTb);             //-时钟低后半个延时
    //-时钟高
    SoftIIC_WeakIO_SCLOut(ID, 1);
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTsclH);
    //-时钟低
    SoftIIC_WeakIO_SCLOut(ID, 0);
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTf);
}

/************************************************|
 * 描述   : 主机非应答信号[主机发起]
 * 版本   : 0.01
 * 日期   : 2018/11/12
 * 函数名 : SoftIIC_NACK
 * 参数   : uchar ID      //-总线ID
 * 返回   : void
 *** 说明 : 主机获取从机数据,反馈从机
 *** 例程 : 无
 ************************************************/
void SoftIIC_NACK(uchar ID)
{
    //-时钟低
    SoftIIC_WeakIO_SDAOut(ID, 1);                       //-应答
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTb);             //-时钟低后半个延时
    //-时钟高
    SoftIIC_WeakIO_SCLOut(ID, 1);
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTsclH);
    //-时钟低
    SoftIIC_WeakIO_SCLOut(ID, 0);
    SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTf);
}

/************************************************|
 * 描述   : 主机发送字节[主机发起]
 * 版本   : 0.01
 * 日期   : 2018/11/12
 * 函数名 : SoftIIC_TxByte
 * 参数   : uchar ID      //-总线ID
 *          uchar Byte
 * 返回   : void
 *** 说明 : 主机发送1字节数据到从机
 *** 例程 : 无
 ************************************************/
void SoftIIC_TxByte(uchar ID, uchar Byte)
{
    uchar i;
//===
    i = 8;
    do{
        //-低电平改变数据
        SoftIIC_WeakIO_SDAOut(ID, !!(Byte&0x80));       //-传输数据
        Byte <<= 1;
        SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTb);
        //-高电平锁定数据
        SoftIIC_WeakIO_SCLOut(ID, 1);
        SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTsclH);      //-SCL高电平时间
        //-低电平
        SoftIIC_WeakIO_SCLOut(ID, 0);
        SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTf);
    }while(--i);
}

/************************************************|
 * 描述   : 主机接收字节[主机发起]
 * 版本   : 0.01
 * 日期   : 2018/11/12
 * 函数名 : SoftIIC_RxByte
 * 参数   : uchar ID      //-总线ID
 *          uchar Byte
 * 返回   : uchar
 *** 说明 : 主机从从机获取1字节
 *** 例程 : 无
 ************************************************/
uchar SoftIIC_RxByte(uchar ID)
{
    uchar Out;
    uchar i;
//===
    SoftIIC_WeakIO_SDARelease(ID, 0);                   //-数据线-释放
    i=8;
    do{
        SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTb);
        //-高电平-获取数据
        SoftIIC_WeakIO_SCLOut(ID, 1);
        SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTsclH);      //-SCL高电平时间
        Out <<= 1;
        Out  |= (0x01 & SoftIIC_WeakIO_SDAIn(ID));
        //-低电平
        SoftIIC_WeakIO_SCLOut(ID, 0);
        SoftIIC_Weak_Delay_us(ID, _SoftIIC_DTf);
    }while(--i);
    SoftIIC_WeakIO_SDARelease(ID, 1);                   //-数据线-控制
    return(Out);
}

/************************************************ 我是分割线 ************************************************/
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/************************************************|
 * 描述   : 读写数据
 * 版本   : 0.01
 * 日期   : 2018/11/13
 * 函数名 : SoftIIC_ReadWrite
 * 参数   :
 *  uchar        ID     //-总线ID
 *  TypeSoftIIC *pData  //-数据
 * 返回   : uchar
 *  +=返回ACK状态
 *  |- 0: ACK
 *  |- 1: NACK
 *** 说明 :
 ***    读写数据在"WriteLen","ReadLen"长度不为0时启动,先写在读;
 ***    读写开始前都会写一次设备地址;
 ***    读写流程入下:
 ***        启动->写设备地址|写->写数据->启动->写设备地址|读->读数据->停止
 ***        启动->写设备地址|写->写数据->停止
 ***        启动->写设备地址|读->读数据->停止
 *** 例程 : 无
 ************************************************/
uchar SoftIIC_ReadWrite(uchar ID, TypeSoftIIC *pData)
{
    uchar State;
    uchar WLen;                                     //-写长度
    uchar RLen;                                     //-读长度
    uchar DevAddr;                                  //-设备地址
//===
    DevAddr = pData->DevAddr & 0xFE;                //-得到纯地址
    //-写
    WLen = pData->WriteLen;
    if(WLen){
        SoftIIC_Start(ID);                          //-启动

        //-设备地址-写
        SoftIIC_TxByte(ID, DevAddr|_DevDir_Write);
        State = SoftIIC_GetSlaveACK(ID);
        if(State){ goto GOTO_End; }

        do{
            SoftIIC_TxByte(ID, *pData->pBuf++);     //-写数据
            State = SoftIIC_GetSlaveACK(ID);
            if(State){ goto GOTO_End; }
        }while(--WLen);
    }
    //-读
    RLen = pData->ReadLen;
    if(RLen){
        SoftIIC_Start(ID);

        //-设备地址-读
        SoftIIC_TxByte(ID, DevAddr|_DevDir_Read);
        State = SoftIIC_GetSlaveACK(ID);
        if(State){ goto GOTO_End; }

        *pData->pBuf++ = SoftIIC_RxByte(ID);        //-读数据
        while(--RLen){
            SoftIIC_ACK(ID);
            *pData->pBuf++ = SoftIIC_RxByte(ID);
        }
        SoftIIC_NACK(ID);
    }
//===
GOTO_End:;
    SoftIIC_Stop(ID);
    return(State);
}

/************************************************|
 * 描述   : 写数据
 * 版本   : 0.01
 * 日期   : 2018/11/13
 * 函数名 : SoftIIC_Write
 * 参数   :
 *  uchar  ID       //-总线ID
 *  uchar  DevAddr  //-设备地址
 *  uchar *pData    //-数据组
 *  uchar  Len      //-数据长度
 * 返回   : uchar
 *  +=返回ACK状态
 *  |- 0: ACK
 *  |- 1: NACK
 *** 说明 : 指定设备写数据
 *** 例程 : 无
 ************************************************/
uchar SoftIIC_Write(uchar ID, uchar DevAddr, uchar *pData, uchar Len)
{
    uchar State;
//===
    //-启动
    SoftIIC_Start(ID);

    //-设备地址-写
    SoftIIC_TxByte(ID, DevAddr|_DevDir_Write);
    State = SoftIIC_GetSlaveACK(ID);
    if(State){ goto GOTO_End; }

    //-写数据
    do{
        SoftIIC_TxByte(ID, *pData++);
        State = SoftIIC_GetSlaveACK(ID);
        if(State){ break; }
    }while(--Len);

    //-停止
GOTO_End:
    SoftIIC_Stop(ID);

    return(State);
}

/************************************************|
 * 描述   : 读数据
 * 版本   : 0.01
 * 日期   : 2018/11/13
 * 函数名 : SoftIIC_Read
 * 参数   :
 *  uchar  ID       //-总线ID
 *  uchar  DevAddr  //-设备地址
 *  uchar *pData    //-数据组
 *  uchar  Len      //-数据长度
 * 返回   : uchar
 *  +=返回ACK状态
 *  |- 0: ACK
 *  |- 1: NACK
 *** 说明 : 指定设备读数据
 *** 例程 : 无
 ************************************************/
uchar SoftIIC_Read(uchar ID, uchar DevAddr, uchar *pData, uchar Len)
{
    uchar State;
//===
    //-启动
    SoftIIC_Start(ID);

    //-设备地址-读
    SoftIIC_TxByte(ID, DevAddr|_DevDir_Read);
    State = SoftIIC_GetSlaveACK(ID);
    if(State){ goto GOTO_End; }

    //-读数据
    *pData++ = SoftIIC_RxByte(ID);                  //-读数据
    while(--Len){
        SoftIIC_ACK(ID);
        *pData++ = SoftIIC_RxByte(ID);
    }
    SoftIIC_NACK(ID);

    //-停止
GOTO_End:
    SoftIIC_Stop(ID);

    return(State);
}

/************************************************|
 * 描述   : 读数据-指定地址(8Bit)
 * 版本   : 0.01
 * 日期   : 2018/11/13
 * 函数名 : SoftIIC_Read_Specify8BitAddr
 * 参数   :
 *  uchar             ID     //-总线ID
 *  TypeSoftIIC_Addr *pt     //-数据类型
 * 返回   : uchar
 *  +=返回ACK状态
 *  |- 0: ACK
 *  |- 1: NACK
 *** 说明 :
 *** 例程 : 无
 ************************************************/
uchar SoftIIC_Read_Specify8BitAddr(uchar ID, TypeSoftIIC_Addr *pt)
{
    uchar State;
    uchar DevAddr;
    uchar Len;
//===
    DevAddr = pt->DevAddr & 0xFE;                   //-得到纯地址

    //-启动
    SoftIIC_Start(ID);
    //-设备地址-写
    SoftIIC_TxByte(ID, DevAddr|_DevDir_Write);
    State = SoftIIC_GetSlaveACK(ID);
    if(State){ goto GOTO_End; }
    //-写地址
    SoftIIC_TxByte(ID, (uchar)pt->Addr);
    State = SoftIIC_GetSlaveACK(ID);
    if(State){ goto GOTO_End; }

    //-启动
    SoftIIC_Start(ID);
    //-设备地址-读
    SoftIIC_TxByte(ID, DevAddr|_DevDir_Read);
    State = SoftIIC_GetSlaveACK(ID);
    if(State){ goto GOTO_End; }
    //-读数据
    *pt->pBuf++ = SoftIIC_RxByte(ID);
    Len = pt->Len;
    while(--Len){
        SoftIIC_ACK(ID);
        *pt->pBuf++ = SoftIIC_RxByte(ID);
    }
    SoftIIC_NACK(ID);

    //-停止
GOTO_End:
    SoftIIC_Stop(ID);
    return(State);
}

/************************************************|
 * 描述   : 写数据-指定地址(8Bit)
 * 版本   : 0.01
 * 日期   : 2018/11/13
 * 函数名 : SoftIIC_Write_Specify8BitAddr
 * 参数   :
 *  uchar             ID     //-总线ID
 *  TypeSoftIIC_Addr *pt     //-数据类型
 * 返回   : uchar
 *  +=返回ACK状态
 *  |- 0: ACK
 *  |- 1: NACK
 *** 说明 :
 *** 例程 : 无
 ************************************************/
uchar SoftIIC_Write_Specify8BitAddr(uchar ID, TypeSoftIIC_Addr *pt)
{
    uchar State;
    uchar DevAddr;
    uchar Len;
//===
    DevAddr = pt->DevAddr & 0xFE;                   //-得到纯地址

    //-启动
    SoftIIC_Start(ID);
    //-设备地址-写
    SoftIIC_TxByte(ID, DevAddr|_DevDir_Write);
    State = SoftIIC_GetSlaveACK(ID);
    if(State){ goto GOTO_End; }
    //-写地址
    SoftIIC_TxByte(ID, (uchar)pt->Addr);
    State = SoftIIC_GetSlaveACK(ID);
    if(State){ goto GOTO_End; }

    //-写数据
    Len = pt->Len;
    do{
        SoftIIC_TxByte(ID, *pt->pBuf++);
        State = SoftIIC_GetSlaveACK(ID);
        if(State){ break; }
    }while(--Len);

    //-停止
GOTO_End:
    SoftIIC_Stop(ID);

    return(State);
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
#if(_SoftIIC_WeakFunc_EN==1)
/*=== 弱函数实现-说明*/
/************************************************|
 * 描述:    [弱函数IO] SDA数据线_IO输出
 * 版本:    0.01
 * 日期:    2018/11/13
 * 函数名:  SoftIIC_WeakIO_SDAOut
 * 形参[I]: uchar ID        //总线ID
 * 形参[I]: uchar HL        //输出IO的高低(0/1)
 * 返回:    void
 *** 说明:  SDA数据线输出高低
 *** 例程:  无
 ************************************************/
__WEAK void SoftIIC_WeakIO_SDAOut(uchar ID, uchar HL)
{}

/************************************************|
 * 描述:    [弱函数IO] SDA数据线_IO输入
 * 版本:    0.01
 * 日期:    2018/11/13
 * 函数名:  SoftIIC_WeakIO_SDAIn
 * 形参[I]: uchar ID        //总线ID
 * 返回:    uchar           //0低;1高
 *** 说明:
 ***    获取SDA数据线电平的高低
 ***    默认得到高;
 *** 例程:  无
 ************************************************/
__WEAK uchar SoftIIC_WeakIO_SDAIn(uchar ID)
{
    return(1);
}


/************************************************|
 * 描述:    [弱函数IO] SDA数据线_配置IO是输入还是输出
 * 版本:    0.01
 * 日期:    2018/11/13
 * 函数名:  SoftIIC_WeakIO_SDARelease
 * 形参[I]: uchar ID        //总线ID
 * 形参[I]: uchar Param
 *  +=总线配置
 *  | 1     //获取总线,配置推挽输出;
 *  | 0     //释放总线,配置浮空输入或者上拉输入;
 * 返回:    void
 *** 说明:  IIC的SDA数据线配置
 *** 例程:  无
 ************************************************/
__WEAK void SoftIIC_WeakIO_SDARelease(uchar ID, uchar Param)
{}

/************************************************|
 * 描述:    [弱函数IO] SCL时钟线_输出高低
 * 版本:    0.01
 * 日期:    2018/11/13
 * 函数名:  SoftIIC_WeakIO_SCLOut
 * 形参[I]: uchar ID        //总线ID
 * 形参[I]: uchar HL        //输出(0/1)
 * 返回:    void
 *** 说明:  IIC的SCL时钟线,输出
 *** 例程:  无
 ************************************************/
__WEAK void SoftIIC_WeakIO_SCLOut(uchar ID, uchar HL)
{}

/************************************************|
 * 描述:    [弱函数] us延时
 * 版本:    0.01
 * 日期:    2018/11/13
 * 函数名:  SoftIIC_Weak_Delay_us
 * 形参[I]: uchar  ID       //总线ID
 * 形参[I]: ushort us       //us延时
 * 返回:    void
 *** 说明:  产生一个us延时
 *** 例程:  无
 ************************************************/
__WEAK void SoftIIC_Weak_Delay_us(uchar ID, ushort us)
{}
//===
#endif
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
